package com.video.service;

import com.video.pojo.Users;

/**
 * @author
 * @date 2020/12/23 - 19:39
 */
public interface UserService {
    /**
     *
     *判断用户名是否为空
     */
    public boolean queryUsernameIsExist(String username);

    /**
     *
     * 保存用户(用户注册)
     */
    public void saveUser(Users users);

    /**
     *
     * 用户登录，根据用户名和密码查询用户
     */
    public Users queryUserForLogin(String username,String password);

}
